package com.fa.ims.util;

import com.fa.ims.enums.InterviewResult;
import com.fa.ims.enums.JobStatus;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EnumUtil {

    public static <T> boolean isValidName (Class<T> clazz, String name) {
        return Arrays.stream(clazz.getEnumConstants()).anyMatch(e -> e.toString().equalsIgnoreCase(name));
    }

    public static JobStatus isValidJobStatus(String name) {
        JobStatus[] values = JobStatus.values();
        List<JobStatus> jobStatusList = Arrays.stream(values)
                .filter(status -> status.getLabel().equalsIgnoreCase(name.trim()))
                .collect(Collectors.toList());
        return jobStatusList.isEmpty() ? null : jobStatusList.get(0);
    }

    public static InterviewResult isValidInterviewResult(String name) {
        InterviewResult[] values = InterviewResult.values();
        List<InterviewResult> interviewResultList = Arrays.stream(values)
                .filter(status -> status.getLabel().equalsIgnoreCase(name.trim()))
                .collect(Collectors.toList());
        return interviewResultList.isEmpty() ? null : interviewResultList.get(0);
    }
}
