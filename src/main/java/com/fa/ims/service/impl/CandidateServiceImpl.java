package com.fa.ims.service.impl;

import com.fa.ims.dto.CandidateDto;
import com.fa.ims.entities.*;
import com.fa.ims.enums.CandidateStatus;
import com.fa.ims.exception.ResourceNotFoundException;
import com.fa.ims.repository.*;
import com.fa.ims.service.CandidateService;
import com.fa.ims.service.FileStorageService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CandidateServiceImpl implements CandidateService {
    private final CandidateRepository candidateRepository;
    private final PositionRepository positionRepository;
    private final UserRepository userRepository;
    private final SkillRepository skillRepository;
    private final CandidateSkillRepository candidateSkillRepository;
    private final FileStorageService fileStorageService;

    @Override
    public Page<CandidateDto> findAll(String search, CandidateStatus status, int page, int size) {
        Specification<Candidate> specification = undeletedSpec();
        if (StringUtils.hasText(search)) {
            String keyword = search.trim().toUpperCase();
            Specification<Candidate> searchByKeyword = (root, query, builder) -> builder.or(
                    builder.like(builder.upper(root.get("fullName")), "%" + keyword + "%"),
                    builder.like(builder.upper(root.get("email")), "%" + keyword + "%"),
                    builder.like(builder.upper(root.get("phoneNumber")), "%" + keyword + "%"),
                    builder.like(builder.upper(root.join("user").get("username")), "%" + keyword + "%"),
                    builder.like(builder.upper(root.join("position").get("name")), "%" + keyword + "%")
            );
            specification = specification.and(searchByKeyword);
        }
        if (status != null) {
            Specification<Candidate> searchByStatus = (root, query, builder) ->
                    builder.equal(root.get("status"), status.ordinal());
            specification = specification.and(searchByStatus);
        }
        Page<Candidate> candidatePage = candidateRepository
                .findAll(specification, PageRequest.of(page, size, Sort.by("lastModifiedDate").descending()));
        return candidatePage.map(CandidateDto::new);
    }

    @Transactional
    @Override
    public Candidate create(CandidateDto candidateDto) throws IOException {
        Candidate candidate = new Candidate();
        candidate.setDeleted(false);
        convertCandidateDtoToEntity(candidateDto, candidate);
        Set<CandidateSkill> candidateSkills = Arrays.stream(candidateDto.getSkills())
                .map(skillId -> new CandidateSkill(candidate,
                        skillRepository.findById(skillId).orElseThrow(ResourceNotFoundException::new),true))
                .collect(Collectors.toSet());
        candidate.setCandidateSkills(candidateSkills);
        if (StringUtils.hasText(candidateDto.getCv().getOriginalFilename())) {
            String cvRelativePath = fileStorageService.saveFile(candidateDto.getCv(), candidateDto.getEmail());
            candidate.setCv(cvRelativePath);
        }
        return candidateRepository.save(candidate);
    }

    @Override
    public CandidateDto getInformationCandidate(Long id) {
        CandidateDto candidateDto = new CandidateDto();
        Candidate candidate = candidateRepository.findByIdAndAndDeletedFalse(id)
                .orElseThrow(ResourceNotFoundException::new);
        BeanUtils.copyProperties(candidate, candidateDto);
        candidateDto.setUriPath(candidate.getCv());
        candidateDto.setUser(candidate.getUser().getUsername());
        candidateDto.setPosition(candidate.getPosition().getName());
        List<CandidateSkill> candidateSkills = candidateSkillRepository.findByCandidateAndIsActiveTrue(candidate);
        Long[] idSkills = candidateSkills.stream()
                .map(CandidateSkill::getSkill)
                .map(Skill::getId)
                .toArray(Long[]::new);
        candidateDto.setSkills(idSkills);
        return candidateDto;
    }

    @Transactional
    @Override
    public void update(CandidateDto candidateDto) {
        Candidate candidate = candidateRepository.findByIdAndAndDeletedFalse(candidateDto.getId())
                .orElseThrow(ResourceNotFoundException::new);
        convertCandidateDtoToEntity(candidateDto, candidate);
        List<Long> skillIds = candidateSkillRepository.findByCandidateAndIsActiveTrue(candidate)
                .stream()
                .map(CandidateSkill::getSkill)
                .map(Skill::getId)
                .collect(Collectors.toList());
        List<Long> newSkillIds = Arrays.stream(candidateDto.getSkills()).collect(Collectors.toList());
        Set<CandidateSkill> candidateSkills = newSkillIds.stream()
                .filter(newSkillId -> !skillIds.contains(newSkillId))
                .map(newSkillId -> new CandidateSkill(candidate, new Skill(newSkillId), true))
                .collect(Collectors.toSet());
        candidate.setCandidateSkills(candidateSkills);
        candidateRepository.save(candidate);
        List<Long> inactiveCandidateSkills = skillIds.stream()
                .filter(skillId -> !newSkillIds.contains(skillId))
                .collect(Collectors.toList());
        inactiveCandidateSkills.forEach(skillId -> {
            CandidateSkill candidateSkill = new CandidateSkill(candidate, new Skill(skillId), false);
            candidateSkillRepository.save(candidateSkill);
        });
    }

    @Transactional
    @Override
    public void delete(Long id) {
        Candidate candidate = candidateRepository.findByIdAndAndDeletedFalse(id)
                .orElseThrow(ResourceNotFoundException::new);
        candidate.setDeleted(true);
        candidateRepository.save(candidate);
        Set<CandidateSkill> candidateSkills = candidate.getCandidateSkills();
        candidateSkills.forEach(candidateSkill -> {
            candidateSkill.setIsActive(false);
            candidateSkillRepository.save(candidateSkill);
        });
    }

    @Override
    public List<Candidate> findAllAndDeletedFalseAndNotExistInterview() {
        return candidateRepository.findAllByDeletedFalseAndInterviewNull();
    }

    @Override
    public boolean existsByEmail(String email) {
        return candidateRepository.existsByEmail(email);
    }

    @Override
    public Optional<Candidate> findById(Long id) {
        return candidateRepository.findById(id);
    }

    private void convertCandidateDtoToEntity(CandidateDto candidateDto, Candidate candidate) {
        BeanUtils.copyProperties(candidateDto, candidate);
        Position position = positionRepository.findByName(candidateDto.getPosition())
                .orElseThrow(ResourceNotFoundException::new);
        candidate.setPosition(position);
        User user = userRepository.findByUsernameAndDeletedFalse(candidateDto.getUser())
                .orElseThrow(ResourceNotFoundException::new);
        candidate.setUser(user);
    }
}
