package com.fa.ims.service;

import com.fa.ims.entities.Position;

import java.util.List;

public interface PositionService {
    List<Position> findAll();
}
