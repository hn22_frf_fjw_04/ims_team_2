package com.fa.ims.enums;

public enum CandidateStatus {
    OPEN("Open"),
    BANNED("Banned");

    private final String label;
    CandidateStatus(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
