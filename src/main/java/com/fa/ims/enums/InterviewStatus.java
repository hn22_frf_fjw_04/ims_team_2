package com.fa.ims.enums;

public enum InterviewStatus {
    OPEN("Open"),
    FAIL("Fail"),
    PASS("Pass"),
    CANCEL("Cancel");
    private final String label;

    InterviewStatus(String label){
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
