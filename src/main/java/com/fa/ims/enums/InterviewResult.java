package com.fa.ims.enums;

public enum InterviewResult {
    PENDING_RESULT("Pending Result"),
    OPEN("Open"),
    FAIL("Fail"),
    PASS("Pass"),
    CANCEL("Cancel");
    private final String label;

    InterviewResult(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}


