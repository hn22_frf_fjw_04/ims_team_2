package com.fa.ims.enums;

public enum Department {
    IT,
    HR,
    FINANCE,
    COMMUNICATION,
    MARKETING,
    ACCOUNTING
}
