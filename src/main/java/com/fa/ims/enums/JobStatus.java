package com.fa.ims.enums;

public enum JobStatus {
    OPEN("Open"),
    IN_PROGRESS("In-Progress"),
    CLOSED("Closed");

    private final String label;

    JobStatus(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
