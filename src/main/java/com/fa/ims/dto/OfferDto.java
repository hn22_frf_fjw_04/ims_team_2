package com.fa.ims.dto;

import com.fa.ims.enums.ContractType;
import com.fa.ims.enums.Department;
import com.fa.ims.enums.OfferStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class OfferDto {
    private Long id;

    @NotNull(message = "{offer.contractType.required}")
    private ContractType contractType;

    @NotNull(message = "{offer.department.required}")
    private Department department;

    @NotNull(message = "{offer.dueDate.required}")
    private LocalDate dueDate;

    @NotNull(message = "{offer.contractStartFrom.required}")
    private LocalDate contractStartFrom;

    @NotNull(message = "{offer.contractStartTo.required}")
    private LocalDate contractStartTo;

    private Double basicSalary;

    private OfferStatus status;

    private String interviewNotes;

    private String note;

    @NotNull(message = "{offer.candidate.required}")
    private Long candidateId;

    @NotNull(message = "{offer.position.required}")
    private Long positionId;

    @NotNull(message = "{offer.level.required}")
    private Long levelId;

    @NotNull(message = "{offer.recruiter.required}")
    private Long recruiterId;

    @NotNull(message = "{offer.manager.required}")
    private Long managerId;
}
