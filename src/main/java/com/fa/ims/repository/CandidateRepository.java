package com.fa.ims.repository;

import com.fa.ims.entities.Candidate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CandidateRepository extends BaseRepository<Candidate, Long> {

    Optional<Candidate> findByIdAndAndDeletedFalse(Long id);
    List<Candidate> findAllByDeletedFalseAndInterviewNull();
    List<Candidate> findAllByDeletedFalse();
    boolean existsByEmail(String email);
}
