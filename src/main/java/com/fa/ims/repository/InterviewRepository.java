package com.fa.ims.repository;

import com.fa.ims.entities.Interview;

import java.util.Optional;

public interface InterviewRepository extends BaseRepository<Interview, Long> {
    Optional<Interview> findByIdAndDeletedFalse(Long id);
}
